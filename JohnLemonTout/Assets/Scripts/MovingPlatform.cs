using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;


//Allows platforms to move throughout level
public class MovingPlatform : MonoBehaviour
{
    public NavMeshAgent navMeshAgent;

    public Transform[] waypoints;

    int m_CurrentWaypointIndex;

    void Start ()
    {
        navMeshAgent.SetDestination (waypoints[0].position);
    }

    void Update ()
    {
        if(navMeshAgent.remainingDistance < navMeshAgent.stoppingDistance)
        {
            m_CurrentWaypointIndex = (m_CurrentWaypointIndex + 1) % waypoints.Length;
            navMeshAgent.SetDestination (waypoints[m_CurrentWaypointIndex].position);
        }
    }
}
