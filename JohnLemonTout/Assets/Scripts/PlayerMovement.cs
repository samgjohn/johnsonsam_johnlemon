using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PlayerMovement : MonoBehaviour
{
    public Transform destination;

    public GameObject player;

    public TextMeshProUGUI countText;

    public float turnSpeed = 20f;

    public bool invulnerable;

    float invulnerableCooldown;

    public float jumpForce = 400;

    public LayerMask groundLayers;

    public CapsuleCollider col;

    public int coins = 0;

    Animator m_Animator;
    Rigidbody m_Rigidbody;
    AudioSource m_AudioSource;
    Vector3 m_Movement;
    Quaternion m_Rotation = Quaternion.identity;

    void Start ()
    {
        m_Animator = GetComponent<Animator> ();
        m_Rigidbody = GetComponent<Rigidbody> ();
        m_AudioSource = GetComponent<AudioSource> ();
    }


    //Gives John momentary invincibility
    void OnTriggerEnter (Collider Enemy){
       if (Enemy.tag == "PickUp"){
           invulnerable = true;
           invulnerableCooldown = 20f;
       }
       print("invulnerable:" + invulnerable);
   }

    void Update(){

        if(player.transform.position.y < -5) {
            this.player.transform.position = destination.position;
        }

        if (invulnerable){
            invulnerableCooldown -= Time.deltaTime;
            print("invulnerableCooldown:" + invulnerableCooldown);
            if (invulnerableCooldown <= 0f){
                invulnerable = false;
            }
        }
        //Gives John a jump
        if (Input.GetKeyDown (KeyCode.Space) && IsGrounded())
        {
        m_Rigidbody.AddForce(Vector3.up * jumpForce);
        }
    }

    private void OnGUI()
    {
        GUI.Label(new Rect(10,10,100,20),"Coins: " + coins);
    }

    //Checks to see if John is on the ground before he can jump
    private bool IsGrounded()
    {
    bool OnGround = Physics.CheckCapsule(col.bounds.center, new Vector3(col.bounds.center.x, col.bounds.min.y, col.bounds.center.z), col.radius * .9f, groundLayers);
    print (OnGround);
    return OnGround;
    }

    void FixedUpdate ()
    {
        float horizontal = Input.GetAxis ("Horizontal");
        float vertical = Input.GetAxis ("Vertical");

        m_Movement.Set(horizontal, 0f, vertical);
        m_Movement.Normalize ();

        bool hasHorizontalInput = !Mathf.Approximately (horizontal, 0f);
        bool hasVerticalInput = !Mathf.Approximately (vertical, 0f);
        bool isWalking = hasHorizontalInput || hasVerticalInput;
        m_Animator.SetBool ("IsWalking", isWalking);

        if (isWalking)
        {
            if (!m_AudioSource.isPlaying)
            {
                m_AudioSource.Play();
            }
        }
        else
        {
            m_AudioSource.Stop ();
        }

        Vector3 desiredForward = Vector3.RotateTowards (transform.forward, m_Movement, turnSpeed * Time.deltaTime, 0f);
        m_Rotation = Quaternion.LookRotation (desiredForward);
    }

    void OnAnimatorMove ()
    {
        m_Rigidbody.MovePosition (m_Rigidbody.position + m_Movement * m_Animator.deltaPosition.magnitude);
        m_Rigidbody.MoveRotation (m_Rotation);
    }
}
